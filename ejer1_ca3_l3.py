# Programar que presente una calificacion cualitativa
# de acuerdo a ciertas equivalentes
#   autor   =   "Michael Vasquez"
#   email   =   "michael.vasquez@unl.edu.ec"

try:
    puntuacion  =   float(input("Introduzca puntuacion:\n"))
    #   validar el rango de la puntuacion
    if puntuacion >= 0  and puntuacion  <=  1.0:
        if puntuacion >=  0.9:
            print("Sobresaliente")
        elif puntuacion >=  0.8:
            print("Notable")
        elif puntuacion >=  0.7:
            print("Bien")
        elif puntuacion >=  0.6:
            print("Insuficiente")
    else:
        print("Puntuacion Incorrecta")
except:
    print("Puntuacion incorrecta")

